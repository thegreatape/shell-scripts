#!/bin/bash
WORD_1=$(shuf -n 1 /usr/share/dict/words | sed "s/'//g")
WORD_2=$(shuf -n 1 /usr/share/dict/words | sed "s/'//g")
RNG=$(echo "$((1000 + RANDOM % 9999))")

echo "$WORD_1$WORD_2$RNG"
