#!/bin/bash
# Set variables here if needed
## Set the how to usage function here
function usage() {
	echo "Password store shell script"
	echo "-a for action. Possible actions are: pass-insert, modify, otp-insert"
	echo "The below are optional but helpful for scripting"
	echo "-d directory (path)"
	echo "-u username"
	echo "-p password"
	echo "-t TOTP key"
}

## Set the generic catchall - if no arg, print usage.
#if [ $# -ne 1 ] ; then
if [ $# -eq 0 ] ; then
	usage
	exit 1
fi
## Else, Get opts
while getopts ":a:u:p:t:d:" opt; do
	case $opt in
		a) action="$OPTARG"
		;;
		u) username="$OPTARG"
		;;
		p) password="$OPTARG"
		;;
		t) totp_key="$OPTARG"
		;;
		d) pass_path="$OPTARG"
		;;
		\?) echo "Invalid option -$OPTARG" >&2
		exit 1
		;;
	esac
done
#####
# Define Functions
## Utils
random-username() {
	word_1=$(shuf -n 1 /usr/share/dict/words | sed "s/'//g")
	word_2=$(shuf -n 1 /usr/share/dict/words | sed "s/'//g")
	random_number=$(echo "$((1000 + RANDOM % 9999))")
	random_username="$word_1$word_2$random_number"
	echo "$random_username"
}

ask-for-user() {
	echo "What username would you like to modify or use?"
	read username
}

ask-for-pass() {
	echo "What password would you like to use?"
	read password
}

ask-for-path() {
	echo "Where is the password stored as a path? without trailing /"
	read pass_path
}

## Actions
pass-insert() {
	if [ -z "$pass_path" ]; then
		ask-for-path
	fi
	if [ -z "$username" ]; then
		echo "No username provided - would you like to generate a random one?"
		select yn in "Yes" "No"; do
			case $yn in
				Yes) username=$(random-username) && echo "Your random username is: $username";
				break
				;;
				No) echo "Enter the username here" && read username;
				break
				;;
			esac
		done
	fi
	if [ -z "$password" ]; then
		echo "No password given - would you like to generate one?"
		select yn in "Yes" "No"; do
			case $yn in
				Yes) pass generate "$pass_path/$username" 12;
				break
				;;
				No) ask-for-pass && echo "$password" | pass insert -m "$pass_path/$username";
				break
				;;
			esac
		done
	fi
}

otp-insert() {
	if [ -z "$pass_path" ]; then	
		ask-for-path	
	fi
	if [ -z "$username" ]; then
		ask-for-user
	fi
	if [ -z "$totp_key" ]; then
		echo "No TOTP given - this can be passed with -t"
		echo "What is the TOTP key?"
		read totp_key
	fi
	otp_url="otpauth://totp/totp-secret?secret=$totp_key&issuer=totp-secret"
	echo "Your otp_url is: $otp_url"
	echo "This has been added to your clipboard"
	echo "Sleeping for 5 seconds..."
	sleep 5
	echo "$otp_url" | wl-copy
	pass edit "$pass_path/$username"
}

pass-modify() {
	if [ -z "$pass_path" ]; then
		ask-for-path
	fi
	if [ -z "$username" ]; then
		ask-for-user	
	fi
	pass edit "$pass_path/$username"
}



## Process opts here
if [ -n "$action" ]; then
	if [ "$action" = "pass-insert" ]; then
		pass-insert
	elif [ "$action" = "otp-insert" ]; then
		otp-insert
	elif [ "$action" = "modify" ]; then
		pass-modify
	fi
fi
